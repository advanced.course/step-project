# Step project

## Technologies:
- html
- css
- js
- scss
- npm
- gulp

## Contributions

### Kyrylo Sulimovsky:
#### Site Header and Top Menu:
- Designed and developed the main header of the site.
- Incorporated a top menu that adjusts to different screen resolutions.
- Implemented a dropdown menu feature for smaller screen sizes, ensuring responsive design.

#### People Are Talking About Fork Section:

- Created the section.
- Ensured that this section is both visually appealing and user-friendly.

#### Gulp Build and Project Skeleton:

- Initiated the project's Gulp build setup, which aids in automating repetitive tasks.
- Laid down the foundational structure or skeleton of the project, ensuring the team has a solid base to work upon.

### Vladislav Lyashenko:

#### Revolutionary Editor Block:

- Developed the section that highlights the revolutionary editor feature of the platform.
- Incorporated buttons, reusing SVG icons.

#### Here is What You Get Section:

- Structured this section to showcase the primary offerings or features of the Fork platform.

#### Fork Subscription Pricing Section:

- Designed the pricing tier structure for the Fork platform.
- Emphasized the third price element to always stand out and appear larger than the others, making it the primary focus for potential customers. This design choice is static and not dependent on hover or click actions.


### Authors:
- [Kyrylo Sulimovsky](https://gitlab.com/genkovich)
- [Vladislav Lyashenko](https://gitlab.com/VladLyashenko2109)

#### [Link to the project](https://gitlab.com/advanced.course/step-project/)
